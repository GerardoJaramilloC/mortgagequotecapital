import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-loan-term',
  templateUrl: './loan-term.component.html',
  styleUrls: ['./loan-term.component.css']
})
export class LoanTermComponent implements OnInit {
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveLoanTerm(loanTerm:number) {
    this._data.saveApplicantLoanTerm(loanTerm);
    this._router.navigate(['/step7'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
