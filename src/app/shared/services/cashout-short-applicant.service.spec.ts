import { TestBed, inject } from '@angular/core/testing';

import { CashoutShortApplicantService } from './cashout-short-applicant.service';

describe('CashoutShortApplicantService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CashoutShortApplicantService]
    });
  });

  it('should be created', inject([CashoutShortApplicantService], (service: CashoutShortApplicantService) => {
    expect(service).toBeTruthy();
  }));
});
