import { TestBed, inject } from '@angular/core/testing';

import { AppstateService } from './appstate.service';

describe('AppstateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppstateService]
    });
  });

  it('should be created', inject([AppstateService], (service: AppstateService) => {
    expect(service).toBeTruthy();
  }));
});
