import { Injectable} from '@angular/core';
import {Applicant} from '../models/applicant.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { AppstateService} from './appstate.service';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

const applicantUrl = 'https://us-central1-mortgage-cuote-capital.cloudfunctions.net/saveToStorage';

@Injectable()
export class CashoutShortApplicantService {

  constructor(
    private _applicant: Applicant
    , private http: HttpClient
    , private _router: Router
    , private _activeRoute: ActivatedRoute
    , private _appState: AppstateService
  ) { }
  saveCashOut(cashOut: number, leadId: string) {
    this._applicant.cashOut = cashOut;
    this._applicant.jornayaToken = leadId;
  }
  savePropertyInfor(homeValue: number,
                    typeOfProperty: string,
                    typeOfLoan: string,
                    mortgageBalance: number,
                    loanTerm: number,
                    interestRate: number,
                    typeOfInterestRate: string,
                    creditScore: string) {
    this._applicant.homeValue = homeValue;
    this._applicant.interestRate = interestRate;
    this._applicant.propertyType = typeOfProperty;
    this._applicant.typeOfLoan = typeOfLoan;
    this._applicant.loanTerm = loanTerm;
    this._applicant.mortgageBalance = mortgageBalance;
    this._applicant.typeOfInterestRate = typeOfInterestRate;
    this._applicant.creditScore = creditScore;
  }
  savePersonalData(isMilitary: boolean,
                   firstName: string,
                   lastName: string,
                   address: string,
                   email: string,
                   zipcode: string,
                   state: string,
                   city: string,
                   phone: string,
                   flow: string
                   ) {
    this._applicant.military = isMilitary ? 1 : 0;
    this._applicant.firstName = firstName;
    this._applicant.lastName = lastName;
    this._applicant.address = address;
    this._applicant.email = email;
    this._applicant.zip = zipcode;
    this._applicant.state = state;
    this._applicant.city = city;
    this._applicant.telephone = phone;
    this._applicant.flow = flow;
    this.saveApplicant().subscribe(
      response => this._appState.loading.next(true),
      err => console.error(err),
      () => {this.excecutePixel(); this._appState.loading.next(false); },
    );
  }
  resetApplicant() {
    this._applicant = new Applicant();
  }
  getApplicant() {
    return this._applicant;
  }
  saveApplicant() {
    const dateNow: Date = new Date();
    this._applicant.applicationUrl = this._router.url;
    this._applicant.applicationDate = dateNow.toISOString();
    this._applicant.utcApplicationDate = dateNow.toUTCString();
    const saveApplicant =  this.http.post(applicantUrl, this._applicant, httpOptions);
    return saveApplicant;
  }
  excecutePixel() {
    const yd = this._activeRoute.snapshot.queryParams['yd'];
    const ydClick = this._activeRoute.snapshot.queryParams['ydClick'];
    const url = 'https://server2server.ydigitalmedia.com/?yd=' + encodeURI(yd) + '&uniqid=' + encodeURI(ydClick);
    if (yd && ydClick) {
      this.ymediaPixel(yd, ydClick, url).subscribe(
        response => console.log('Pixel Fired'),
        err => {console.error(err); },
        () => true,
      );
    }

  }
  ymediaPixel(yd, ydClick, url) {
    console.log('service fire')
    const yMediaPixel =  this.http.get(url, httpOptions);
    return yMediaPixel;
  }
}
