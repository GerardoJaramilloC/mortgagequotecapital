import { Injectable } from '@angular/core';
import {Applicant} from '../models/applicant.model';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class UrlhelperService {

  constructor() { }

  convertApplicantToQuery(applicant: Applicant) {
    let httpParams = new HttpParams();
    Object.keys(applicant).forEach(function (key) {
      httpParams = httpParams.append(key, applicant[key]);
    });
    return JSON.stringify(httpParams);
  }

}
