import { Injectable } from '@angular/core';
import {Applicant} from '../models/applicant.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { AppstateService} from './appstate.service';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

const applicantUrl = 'https://us-central1-mortgage-cuote-capital.cloudfunctions.net/saveToStorage';

@Injectable()
export class ApplicantService {

  applicant = new Applicant();
  constructor(
    private http: HttpClient
    , private _router: Router
    , private _activeRoute: ActivatedRoute
    , private _appState: AppstateService
  ) { }
  saveApplicantPhone(telephone: string, leadIdToken: string) {
    this.applicant.telephone = telephone;
    this.applicant.jornayaToken = leadIdToken;
  }
  saveApplicantName(firstName: string, lastName: string) {
    this.applicant.firstName = firstName;
    this.applicant.lastName = lastName;
  }
  saveApplicantEmail(email: string) {
    this.applicant.email = email;
  }
  saveApplicantAddress(
    address: string,
    city: string,
    state: string,
    zipcode: string) {
    this.applicant.address = address;
    this.applicant.city = city;
    this.applicant.state = state;
    this.applicant.zip = zipcode;
  }
  saveApplicantPropertyInfo(
    mortgageBalance: number,
    homeValue: number,
  ) {
    this.applicant.mortgageBalance = mortgageBalance;
    this.applicant.homeValue = homeValue;
  }
  saveApplicantInterest(
    interestRate: number,
    interestType: string) {
    this.applicant.interestRate = interestRate;
    this.applicant.typeOfInterestRate = interestType;
  }
  saveApplicantLoanTerm(
    loanTern: number
  ) {
    this.applicant.loanTerm = loanTern;
  }
  saveCreditScore(score: string) {
    this.applicant.creditScore = score;
  }
  saveCashOut(cashOut: number) {
    this.applicant.cashOut = cashOut;
  }
  saveLoanType(loanType: string) {
    this.applicant.typeOfLoan = loanType;
  }
  saveMilitart(military: number) {
    this.applicant.military = military;
  }
  savePropertyType(propertyType: string, channel: string, aid: string) {
    this.applicant.propertyType = propertyType;
    this.applicant.channel = channel;
    this.applicant.affiliateid = aid;
    this.saveApplicant().subscribe(
      response => this._appState.loading.next(true),
      err => console.error(err),
      () => {this.excecutePixel(); this._appState.loading.next(false); },
    );
  }
  resetApplicant() {
    this.applicant = new Applicant();
  }
  getApplicant() {
    return this.applicant;
  }
  saveApplicant() {
    const dateNow: Date = new Date();
    this.applicant.applicationUrl = this._router.url;
    this.applicant.applicationDate = dateNow.toISOString();
    this.applicant.utcApplicationDate = dateNow.toUTCString();
    const saveApplicant =  this.http.post(applicantUrl, this.applicant, httpOptions);
    return saveApplicant;
  }
  excecutePixel() {
    const yd = this._activeRoute.snapshot.queryParams['yd'];
    const ydClick = this._activeRoute.snapshot.queryParams['ydClick'];
    const url = 'https://server2server.ydigitalmedia.com/?yd=' + encodeURI(yd) + '&uniqid=' + encodeURI(ydClick);
    if (yd && ydClick) {
      this.ymediaPixel(yd, ydClick, url).subscribe(
        response => console.log('Pixel Fired'),
        err => {console.error(err); },
        () => true,
      );
    }

  }
  ymediaPixel(yd, ydClick, url) {
    console.log('service fire')
    const yMediaPixel =  this.http.get(url, httpOptions);
    return yMediaPixel;
  }
}
