export class Applicant {
  $key: string;
  telephone: string;
  firstName: string;
  lastName: string;
  email: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  mortgageBalance: number;
  homeValue: number;
  interestRate: number;
  typeOfInterestRate: string;
  loanTerm: number;
  creditScore: string;
  cashOut: number;
  typeOfLoan: string;
  military: number;
  propertyType: string;
  applicationDate: string;
  utcApplicationDate: string;
  applicationUrl: string;
  jornayaToken: string;
  channel: string;
  affiliateid: string;
  flow: string;
}
