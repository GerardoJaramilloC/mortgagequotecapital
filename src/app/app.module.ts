import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormComponent } from './form/form.component';
import { Step1Component } from './step1/step1.component';
import { PersonalDataComponent } from './personal-data/personal-data.component';
import { EmailComponent } from './email/email.component';
import { AddressComponent } from './address/address.component';
import { PropertyInfoComponent } from './property-info/property-info.component';
import { InterestRateComponent } from './interest-rate/interest-rate.component';
import { LoanTermComponent } from './loan-term/loan-term.component';
import { CreditScoreComponent } from './credit-score/credit-score.component';
import { CashOutComponent } from './cash-out/cash-out.component';
import { LoanTyoeComponent } from './loan-tyoe/loan-tyoe.component';
import { MilitaryComponent } from './military/military.component';
import { PropertyTypeComponent } from './property-type/property-type.component';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { PhoneHomeComponent } from './phone-home/phone-home.component';
import { InfoComponent } from './info/info.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { NgxMaskModule } from 'ngx-mask';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule, MatSliderModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import { LoanTypeComponent } from './loan-type/loan-type.component';
import {ApplicantService} from './shared/services/applicant.service';
import { FinalStepComponent } from './final-step/final-step.component';
import {HttpClientModule} from '@angular/common/http';
import {AppstateService} from './shared/services/appstate.service';
import { HomeComponent } from './cashout/home/home.component';
import { PersonalInfoComponent } from './cashout/personal-info/personal-info.component';
import { PropertyInfoComponent as CashoutPropertyInfo} from './cashout/property-info/property-info.component';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {CashoutShortApplicantService} from './shared/services/cashout-short-applicant.service';
import {FinalStepComponent as CashoutFinalStep} from './cashout/final-step/final-step.component';
import {Applicant} from './shared/models/applicant.model';
import { CashoutMedicalComponent } from './cashout-medical/cashout-medical.component';
import { HomeDebtComponent } from './cashout-consolidation/home-debt/home-debt.component';
import { FinalStepDebtComponent } from './cashout-consolidation/final-step-debt/final-step-debt.component';
import { PersonalInfoDebtComponent } from './cashout-consolidation/personal-info-debt/personal-info-debt.component';
import { PropertyInfoDebtComponent } from './cashout-consolidation/property-info-debt/property-info-debt.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FormComponent,
    Step1Component,
    PersonalDataComponent,
    EmailComponent,
    AddressComponent,
    PropertyInfoComponent,
    InterestRateComponent,
    LoanTermComponent,
    CreditScoreComponent,
    CashOutComponent,
    LoanTyoeComponent,
    MilitaryComponent,
    PropertyTypeComponent,
    NotFoundComponent,
    PhoneHomeComponent,
    InfoComponent,
    PrivacyComponent,
    LoanTypeComponent,
    FinalStepComponent,
    HomeComponent,
    PersonalInfoComponent,
    CashoutPropertyInfo,
    CashoutFinalStep,
    CashoutMedicalComponent,
    HomeDebtComponent,
    FinalStepDebtComponent,
    PersonalInfoDebtComponent,
    PropertyInfoDebtComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxMaskModule.forRoot(),
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    CurrencyMaskModule
  ],
  providers: [ApplicantService, AppstateService, CashoutShortApplicantService, Applicant],
  bootstrap: [AppComponent]
})
export class AppModule { }
