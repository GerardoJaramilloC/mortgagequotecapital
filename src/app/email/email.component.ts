import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  email: string;
  validateEmail = true;
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveEmail() {
    if (this.email) {
      const regexp =
        new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      const emailValidation = regexp.test(this.email);
      if (emailValidation) {
        this._data.saveApplicantEmail(this.email);
        this._router.navigate(['/step3'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
      } else {
        this.validateEmail = false;
      }
    } else {
      this.validateEmail = false;
    }
  }

}
