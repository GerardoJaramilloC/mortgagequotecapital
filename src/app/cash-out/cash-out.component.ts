import { Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-cash-out',
  templateUrl: './cash-out.component.html',
  styleUrls: ['./cash-out.component.css']
})
export class CashOutComponent implements OnInit {
  autoTicks = true;
  disabled = false;
  invert = false;
  max = 200000;
  min = 0;
  min2 = 0;
  showTicks = true;
  step = 10000;
  thumbLabel = false;
  cashOut = 0;
  vertical = false;
  thickInterval = 50000;
  constructor(private _router: Router, private _data: ApplicantService, private _ref: ChangeDetectorRef) { }

  ngOnInit() {
  }
  saveCashOut() {
    this._data.saveCashOut(this.cashOut);
    this._router.navigate(['/step9'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
