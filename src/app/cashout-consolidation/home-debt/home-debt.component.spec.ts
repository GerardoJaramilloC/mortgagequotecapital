import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDebtComponent } from './home-debt.component';

describe('HomeDebtComponent', () => {
  let component: HomeDebtComponent;
  let fixture: ComponentFixture<HomeDebtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDebtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDebtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
