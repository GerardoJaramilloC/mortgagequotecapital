import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CashoutShortApplicantService as ApplicantService} from '../../shared/services/cashout-short-applicant.service';
import {AppstateService} from '../../shared/services/appstate.service';

@Component({
  selector: 'app-home-debt',
  templateUrl: './home-debt.component.html',
  styleUrls: ['./home-debt.component.css']
})
export class HomeDebtComponent implements OnInit {
  h1Title = 'Replace multiple loans with one';
  h2Title = 'low rate monthly payment and simplify your life.';
  h3Title = 'How much money do you owe?';
  h3Title2 = 'Let us find the best quote for you!';
  buttonText = 'Get a Quote';
  cashout: number;
  validateCashOut = true;
  leadid_token: any;
  constructor(private  _router: Router, private _data: ApplicantService, private _app: AppstateService ) { }

  ngOnInit() {
    this._app.homeLink.next('/cashout-debt');
  }
  savePhone(leadid) {
    if (this.cashout && this.cashout.toString().length > 4) {
      this._data.resetApplicant();
      this._data.saveCashOut(this.cashout, leadid);
      this._router.navigate(['/cashout-debt/step2'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    } else {
      this.validateCashOut = false;
    }
  }
}
