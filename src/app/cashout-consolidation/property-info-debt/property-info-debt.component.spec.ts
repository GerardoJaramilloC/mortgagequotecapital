import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyInfoDebtComponent } from './property-info-debt.component';

describe('PropertyInfoDebtComponent', () => {
  let component: PropertyInfoDebtComponent;
  let fixture: ComponentFixture<PropertyInfoDebtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyInfoDebtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyInfoDebtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
