import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalStepDebtComponent } from './final-step-debt.component';

describe('FinalStepDebtComponent', () => {
  let component: FinalStepDebtComponent;
  let fixture: ComponentFixture<FinalStepDebtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalStepDebtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalStepDebtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
