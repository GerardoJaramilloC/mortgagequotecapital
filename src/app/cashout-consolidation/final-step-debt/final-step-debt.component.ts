import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Applicant} from '../../shared/models/applicant.model';
import {CashoutShortApplicantService as ApplicantService} from '../../shared/services/cashout-short-applicant.service';
import {AppstateService} from '../../shared/services/appstate.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-final-step-debt',
  templateUrl: './final-step-debt.component.html',
  styleUrls: ['./final-step-debt.component.css']
})
export class FinalStepDebtComponent implements OnInit, OnDestroy {
  applicant: Applicant;
  loading = true;
  myLoadingSubscription: Subscription;
  homeLink = '';
  constructor(private _activeRoute: ActivatedRoute
    , private _http: HttpClient
    , private _applicant: ApplicantService
    , private _appState: AppstateService
    , private _ref: ChangeDetectorRef) {
    this.applicant = _applicant.getApplicant();
  }

  ngOnInit() {
    this.myLoadingSubscription = this._appState.loading.subscribe(
      (loadingState: boolean) => {
        this.loading = loadingState;
        this._ref.detectChanges();
      }
    );
  }
  ngOnDestroy() {
    this.myLoadingSubscription.unsubscribe();
  }
}
