import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CashoutShortApplicantService as ApplicantService} from '../../shared/services/cashout-short-applicant.service';
import {Location} from '@angular/common';
import {AppstateService} from '../../shared/services/appstate.service';

@Component({
  selector: 'app-personal-info-debt',
  templateUrl: './personal-info-debt.component.html',
  styleUrls: ['./personal-info-debt.component.css']
})
export class PersonalInfoDebtComponent implements OnInit {

  isMilitary = false;
  firstName: string;
  validateFirstName = true;
  lastName: string
  validateLastName = true;
  address: string
  validateAddress = true;
  email: string;
  validateEmail = true;
  zipcode: string;
  validateZipcode = true;
  state: string;
  validateState = true;
  city: string;
  validateCity = true;
  phone: string;
  validatePhone = true;
  constructor(private _router: Router, private _data: ApplicantService, private _location: Location, private _app: AppstateService) { }

  ngOnInit() {
    this._app.homeLink.next('/cashout-debt');
  }

  saveMilitary(isMilitary: boolean) {
    this.isMilitary = isMilitary;
  }
  saveLead() {
    const addressRegEx = new RegExp(/^(\d{3,})\s?(\w{0,5})\s([a-zA-Z]{2,30})\s([a-zA-Z]{2,15})\.?\s?(\w{0,5})$/);
    const cityRegEx = new RegExp(/[a-zA-Z]{2,5}/);
    const zipRegex = new RegExp(/(^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$)/);
    const regexp = new RegExp(/[a-zA-Z]{2,5}/);
    const regexpEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    this.validateCity = this.city && cityRegEx.test(this.city);
    this.validateAddress = this.address && addressRegEx.test(this.address);
    this.validateState = this.state !== undefined;
    this.validateZipcode = this.zipcode && zipRegex.test(this.zipcode);
    this.validateFirstName = this.firstName && this.firstName.length > 2 && regexp.test(this.firstName);
    this.validateLastName = this.lastName && this.lastName.length > 2  && regexp.test(this.lastName);
    this.validateEmail = this.email && this.email.length > 2  && regexpEmail.test(this.email);
    this.validatePhone = this.phone && this.phone.toString().length > 5;
    if (this.validateFirstName &&
      this.validateLastName &&
      this.validateAddress &&
      this.validateEmail &&
      this.validateZipcode &&
      this.validateState &&
      this.validateCity &&
      this.validatePhone) {
      this._data.savePersonalData(
        this.isMilitary,
        this.firstName,
        this.lastName,
        this.address,
        this.email,
        this.zipcode,
        this.state,
        this.city,
        this.phone,
        'Debt Consolidation');
      this._router.navigate(['/cashout-debt/thankyou'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    } else {
      console.log('no lead');
    }
  }
  goBack() {
    this._location.back();
  }

}
