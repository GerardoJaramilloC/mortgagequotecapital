import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalInfoDebtComponent } from './personal-info-debt.component';

describe('PersonalInfoDebtComponent', () => {
  let component: PersonalInfoDebtComponent;
  let fixture: ComponentFixture<PersonalInfoDebtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalInfoDebtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalInfoDebtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
