import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';


@Component({
  selector: 'app-phone-home',
  templateUrl: './phone-home.component.html',
  styleUrls: ['./phone-home.component.css']
})
export class PhoneHomeComponent implements OnInit {
  phone: string;
  validatePhone = true;
  leadid_token: any;
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  savePhone(leadid) {
    if (this.phone && this.phone.length > 9) {
      this._data.resetApplicant();
      this._data.saveApplicantPhone(this.phone, leadid);
      this._router.navigate(['/step1'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    } else {
      this.validatePhone = false;
    }
  }
}
