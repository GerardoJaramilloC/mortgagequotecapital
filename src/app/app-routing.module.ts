import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {PhoneHomeComponent} from './phone-home/phone-home.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {PersonalDataComponent} from './personal-data/personal-data.component';
import {EmailComponent} from './email/email.component';
import {AddressComponent} from './address/address.component';
import {PropertyInfoComponent} from './property-info/property-info.component';
import {InterestRateComponent} from './interest-rate/interest-rate.component';
import {LoanTermComponent} from './loan-term/loan-term.component';
import {CreditScoreComponent} from './credit-score/credit-score.component';
import {CashOutComponent} from './cash-out/cash-out.component';
import {LoanTypeComponent} from './loan-type/loan-type.component';
import {MilitaryComponent} from './military/military.component';
import {PropertyTypeComponent} from './property-type/property-type.component';
import {FinalStepComponent} from './final-step/final-step.component';
import {HomeComponent as CashoutHome} from './cashout/home/home.component';
import {PersonalInfoComponent as CashoutPersoanlInfo} from './cashout/personal-info/personal-info.component';
import {PropertyInfoComponent as CashoutPropertyInfo} from './cashout/property-info/property-info.component';
import {FinalStepComponent as CashoutFinalStep} from './cashout/final-step/final-step.component';
import {HomeDebtComponent as CashoutDebt} from './cashout-consolidation/home-debt/home-debt.component';
import { FinalStepDebtComponent } from './cashout-consolidation/final-step-debt/final-step-debt.component';
import { PersonalInfoDebtComponent } from './cashout-consolidation/personal-info-debt/personal-info-debt.component';
import { PropertyInfoDebtComponent } from './cashout-consolidation/property-info-debt/property-info-debt.component';

const routes: Routes = [
  {path: '', component: PhoneHomeComponent, pathMatch: 'full'},
  {path: 'step1', component: PersonalDataComponent, pathMatch: 'full'},
  {path: 'step2', component: EmailComponent, pathMatch: 'full'},
  {path: 'step3', component: AddressComponent, pathMatch: 'full'},
  {path: 'step4', component: PropertyInfoComponent, pathMatch: 'full'},
  {path: 'step5', component: InterestRateComponent, pathMatch: 'full'},
  {path: 'step6', component: LoanTermComponent, pathMatch: 'full'},
  {path: 'step7', component: CreditScoreComponent, pathMatch: 'full'},
  {path: 'step8', component: CashOutComponent, pathMatch: 'full'},
  {path: 'step9', component: LoanTypeComponent, pathMatch: 'full'},
  {path: 'step10', component: MilitaryComponent, pathMatch: 'full'},
  {path: 'step11', component: PropertyTypeComponent, pathMatch: 'full'},
  {path: 'finalstep', component: FinalStepComponent, pathMatch: 'full'},
  {path: 'cashout-college', children: [
      {path: '', component: CashoutHome, pathMatch: 'full'},
      {path: 'step2', component: CashoutPropertyInfo, pathMatch: 'full'},
      {path: 'step3', component: CashoutPersoanlInfo, pathMatch: 'full'},
      {path: 'thankyou', component: CashoutFinalStep, pathMatch: 'full'}
    ]
  },
  {path: 'cashout-debt', children: [
      {path: '', component: CashoutDebt, pathMatch: 'full'},
      {path: 'step2', component: PropertyInfoDebtComponent, pathMatch: 'full'},
      {path: 'step3', component: PersonalInfoDebtComponent, pathMatch: 'full'},
      {path: 'thankyou', component: FinalStepDebtComponent, pathMatch: 'full'}
    ]},
  {path: 'privacy', component: PrivacyComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
