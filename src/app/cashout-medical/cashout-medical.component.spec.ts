import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashoutMedicalComponent } from './cashout-medical.component';

describe('CashoutMedicalComponent', () => {
  let component: CashoutMedicalComponent;
  let fixture: ComponentFixture<CashoutMedicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashoutMedicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashoutMedicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
