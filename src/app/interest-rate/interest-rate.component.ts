import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';
import {Applicant} from '../shared/models/applicant.model';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-interest-rate',
  templateUrl: './interest-rate.component.html',
  styleUrls: ['./interest-rate.component.css']
})
export class InterestRateComponent implements OnInit {
  interestRate: number;
  validateInterestRate = true;
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveInterestRate(typeOfInterest: string) {
    if (this.interestRate) {
      this._data.saveApplicantInterest(this.interestRate, typeOfInterest);
      this._router.navigate(['/step6'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    } else {
      this.validateInterestRate = false;
    }
  }
}
