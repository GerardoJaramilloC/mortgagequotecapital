import { Component, OnInit } from '@angular/core';
import {Router, RouterLink} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';
import {Applicant} from '../shared/models/applicant.model';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  city: string;
  address: string;
  state: string;
  zip: string;
  validateCity = true;
  validateAddress = true;
  validateState = true;
  validateZip = true;
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveAddress() {
    const addressRegEx = new RegExp(/^(\d{3,})\s?(\w{0,5})\s([a-zA-Z]{2,30})\s([a-zA-Z]{2,15})\.?\s?(\w{0,5})$/);
    const cityRegEx = new RegExp(/[a-zA-Z]{2,5}/);
    const zipRegex = new RegExp(/(^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$)/);
    this.validateCity = this.city && cityRegEx.test(this.city);
    this.validateAddress = this.address && addressRegEx.test(this.address);
    this.validateState = this.state !== undefined;
    this.validateZip = this.zip && zipRegex.test(this.zip);
    if (this.validateCity && this.validateAddress && this.validateState && this.validateZip) {
      this._data.saveApplicantAddress(this.address, this.city, this.state, this.zip);
      this._router.navigate(['/step4'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    }
  }
}
