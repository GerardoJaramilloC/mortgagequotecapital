import { Renderer2, OnInit, Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  constructor(private _renderer2: Renderer2, _elRef: ElementRef) {}
  public ngOnInit() {
    const s = document.createElement('script');
    s.id = 'LeadiDscript_campaign';
    s.type = 'text/javascript';
    s.async = true;
    s.src = '//create.lidstatic.com/campaign/5aefb740-686b-f8b1-582c-00f46ca320c5.js?snippet_version=2';
    const LeadiDscript = document.getElementById('LeadiDscript');
    this._renderer2.appendChild(document.body, s);
  }
}

