import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-loan-type',
  templateUrl: './loan-type.component.html',
  styleUrls: ['./loan-type.component.css']
})
export class LoanTypeComponent implements OnInit {

  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveLoanType(loanType) {
    this._data.saveLoanType(loanType);
    this._router.navigate(['/step10'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
