import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-property-type',
  templateUrl: './property-type.component.html',
  styleUrls: ['./property-type.component.css']
})
export class PropertyTypeComponent implements OnInit {
  channel: string;
  aid: string;

  constructor(private _router: Router, private _data: ApplicantService, private _activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.channel = this._activeRoute.snapshot.queryParams['channel'] || 'NA';
    this.aid = this._activeRoute.snapshot.queryParams['aid'] || 'NA';
  }
  savePropertyType(propertyType) {
    this._data.savePropertyType(propertyType, this.channel, this.aid);
    this._router.navigate(['/finalstep'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
