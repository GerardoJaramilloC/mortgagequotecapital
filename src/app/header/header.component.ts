import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AppstateService} from '../shared/services/appstate.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  phoneNumber = '8442626240';
  homeLink: string;
  constructor(private _routeActivated: ActivatedRoute, private _app: AppstateService) {}

  ngOnInit() {
    this._app.homeLink.subscribe(
      (data: string) => { this.homeLink = data; }
    );
  }
  ngOnDestroy() {}

}
