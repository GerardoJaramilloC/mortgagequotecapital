import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-property-info',
  templateUrl: './property-info.component.html',
  styleUrls: ['./property-info.component.css'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false,
})

export class PropertyInfoComponent implements OnInit {
  autoTicks = true;
  disabled = false;
  invert = false;
  max = 1000000;
  min = 0;
  min2 = 60000;
  showTicks = true;
  step = 50000;
  thumbLabel = false;
  mortgageBalance = 0;
  homeValue = 160000;
  vertical = false;
  thickInterval = 50000;
  constructor(private _router: Router, private _data: ApplicantService) {}

  ngOnInit() {
  }

  savePropertyInfo() {
    this._data.saveApplicantPropertyInfo(this.mortgageBalance, this.homeValue);
    this._router.navigate(['/step5'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
