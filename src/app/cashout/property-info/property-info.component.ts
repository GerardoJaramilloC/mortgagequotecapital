import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CashoutShortApplicantService as ApplicantService} from '../../shared/services/cashout-short-applicant.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-property-info',
  templateUrl: './property-info.component.html',
  styleUrls: ['./property-info.component.css']
})
export class PropertyInfoComponent implements OnInit {

  homeValue: number;
  validateHomeValue = true;
  typeOfProperty: string;
  validateTypeOfProperty = true;
  typeOfLoan: string;
  validateTypeOfLoan = true;
  mortgageBalance: number;
  validateMortgageBalance = true;
  loanTerm: number;
  validateLoanTerm = true;
  interestRate: number;
  validateInterestRate = true;
  typeOfInterestRate = 'Fixed';
  creditScore = 'Excellent';
  displayError = false;
  errorMessag: string;
  constructor(private _router: Router, private _data: ApplicantService, private _location: Location) { }

  ngOnInit() {
  }

  saveTypeInterestRate(typeOfInterest: string) {
    this.typeOfInterestRate = typeOfInterest;
  }

  closeAlert() {
    this.displayError = false;
  }

  saveCreditScore(creditScore: string) {
    this.creditScore = creditScore;
  }
  calculateLtv() {
    if ( this.mortgageBalance && this.homeValue ) {
      const ltv = (this.mortgageBalance / this.homeValue) * 100;
      if (ltv > 0 && ltv <= 100) {
        this.displayError = false;
      } else {
        this.displayError = true;
        this.errorMessag = 'You own more than the value of your House';
      }
    }
  }

  savePropertyInfo() {
    const regexp =
      new RegExp(/[a-zA-Z]{2,5}/);
    this.validateHomeValue = this.homeValue && this.homeValue.toString().length > 4;
    this.validateTypeOfProperty = this.typeOfProperty && this.typeOfProperty.toString().length > 2;
    this.validateTypeOfLoan = this.typeOfLoan && this.typeOfLoan.toString().length > 2;
    this.validateMortgageBalance = this.mortgageBalance && this.mortgageBalance.toString().length > 2;
    this.validateInterestRate = this.interestRate && this.interestRate.toString().length > 2;
    this.validateLoanTerm = this.loanTerm && this.loanTerm.toString().length >= 1 ;
    if (
      this.validateHomeValue &&
      this.validateTypeOfProperty &&
      this.validateTypeOfLoan &&
      this.validateMortgageBalance &&
      this.validateLoanTerm &&
      this.validateInterestRate &&
      !this.displayError
    ) {
      this._data.savePropertyInfor(
        this.homeValue,
        this.typeOfProperty,
        this.typeOfLoan,
        this.mortgageBalance,
        this.loanTerm,
        this.interestRate,
        this.typeOfInterestRate,
        this.creditScore
      );
      this._router.navigate(['/cashout-college/step3'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    }
  }
  goback() {
    this._location.back();
  }
}
