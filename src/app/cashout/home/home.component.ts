import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CashoutShortApplicantService as ApplicantService} from '../../shared/services/cashout-short-applicant.service';
import {AppstateService} from '../../shared/services/appstate.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  h1Title = 'Lower the interest rate of your Student Loan Debt!';
  h2Title = 'Consolidate your student loans and save.';
  h3Title = 'How much money do you own?';
  h3Title2 = 'Let us find the best quote for you!';
  buttonText = 'Lower your Interest';
  cashout: number;
  validateCashOut = true;
  leadid_token: any;
  constructor(private _router: Router, private _data: ApplicantService, private _app: AppstateService) {}

  ngOnInit() {
    console.log(this._app.homeLink);
  }
  savePhone(leadid) {
    if (this.cashout && this.cashout.toString().length > 4) {
      this._data.resetApplicant();
      this._data.saveCashOut(this.cashout, leadid);
      this._router.navigate(['/cashout-college/step2'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    } else {
      this.validateCashOut = false;
    }
  }

}
