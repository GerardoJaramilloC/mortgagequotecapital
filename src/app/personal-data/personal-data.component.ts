import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit {
  firstName: string;
  lastName: string;
  validateFirstName = true;
  validateLastName = true;
  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveName() {
    const regexp =
      new RegExp(/[a-zA-Z]{2,5}/);
    this.validateFirstName = this.firstName && this.firstName.length > 2 && regexp.test(this.firstName);
    this.validateLastName = this.lastName && this.lastName.length > 2  && regexp.test(this.lastName);
    if (this.validateLastName && this.validateFirstName) {
      this._data.saveApplicantName(this.firstName, this.lastName);
      this._router.navigate(['/step2'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
    }
  }
}
