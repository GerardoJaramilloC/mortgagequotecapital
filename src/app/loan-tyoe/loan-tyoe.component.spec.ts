import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanTyoeComponent } from './loan-tyoe.component';

describe('LoanTyoeComponent', () => {
  let component: LoanTyoeComponent;
  let fixture: ComponentFixture<LoanTyoeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanTyoeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanTyoeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
