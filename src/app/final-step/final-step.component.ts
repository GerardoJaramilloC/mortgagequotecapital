import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy} from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Applicant} from '../shared/models/applicant.model';
import {ApplicantService} from '../shared/services/applicant.service';
import {AppstateService} from '../shared/services/appstate.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-final-step',
  templateUrl: './final-step.component.html',
  styleUrls: ['./final-step.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class FinalStepComponent implements OnInit, OnDestroy {
  applicant: Applicant;
  loading = true;
  myLoadingSubscription: Subscription;
  constructor(
    private _activeRoute: ActivatedRoute
    , private _http: HttpClient
    , private _applicant: ApplicantService
    , private _appState: AppstateService
    , private _ref: ChangeDetectorRef,
  ) {
    this.applicant = _applicant.getApplicant();
  }

  ngOnInit() {
    this.myLoadingSubscription = this._appState.loading.subscribe(
      (loadingState: boolean) => {
        this.loading = loadingState;
        this._ref.detectChanges();
      }
    );
  }
  ngOnDestroy() {
    this.myLoadingSubscription.unsubscribe();
  }
}
