import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-credit-score',
  templateUrl: './credit-score.component.html',
  styleUrls: ['./credit-score.component.css']
})
export class CreditScoreComponent implements OnInit {

  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveCreditScore(creditScore) {
    this._data.saveCreditScore(creditScore);
    this._router.navigate(['/step8'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
