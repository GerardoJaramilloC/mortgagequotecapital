import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ApplicantService} from '../shared/services/applicant.service';

@Component({
  selector: 'app-military',
  templateUrl: './military.component.html',
  styleUrls: ['./military.component.css']
})
export class MilitaryComponent implements OnInit {

  constructor(private _router: Router, private _data: ApplicantService) { }

  ngOnInit() {
  }
  saveMilitary(military) {
    this._data.saveMilitart(military);
    this._router.navigate(['/step11'], { queryParams: this._data.getApplicant(), queryParamsHandling: 'merge'});
  }
}
